#ifndef CHART_SQLITE_OP_H
#define CHART_SQLITE_OP_H

#include "sqlite3.h"
#include "utility.h"
#include <sys/types.h>

	
#define CHART_TABLE_NAME "chart"
#define LIMIT_HISTORY_NUM 5        //历史消息条数
#ifdef __cplusplus
	extern "C" {
#endif

enum CHART_MESSAGE_TYPE
{
    E_SYSTEM = 0,   //系统消息，比如加入或创建会话
    E_TEXT,         //普通文字信息
    E_FILE,         //文件类型的消息
};


int insert1ChartRecord(u_int64_t chartId,u_int64_t userId, const char *name, const char *content);
int getDialogList(u_int64_t id,u_int64_t  *resultNum,char **ppDialogList);
int joinInDialog(u_int64_t userId,u_int64_t chartId);
int getDialogPersonList(u_int64_t id,u_int64_t  *resultNum,char **ppDialogList);
int getHistoryMessage(u_int64_t id,u_int64_t  *resultNum,char **ppDialogList);



#ifdef __cplusplus
}
#endif

#endif  //!CHART_SQLITE_OP_H


