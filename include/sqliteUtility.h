#ifndef SQLITE_UTILITY_H
#define SQLITE_UTILITY_H

#include "KernelList.h"
#include "utility.h"

typedef struct _ONE_PERSON_
{
    u_int64_t userId;
    u_int64_t joinTime;
    char name[NAME_LEN];
    struct list_head node;
}ONE_PERSON;
typedef struct _ONE_MESSAGE_
{
    u_int64_t userId;           //发送者id
    char name[NAME_LEN];        //发送者姓名
    u_int64_t sendTime;         //发送时间
    u_int64_t messageType;      //消息类型
    char content[CONTENT_LEN];  //消息内容
    struct list_head node;
}ONE_MESSAGE;
typedef struct _DIALOG_LIST_
{
    u_int64_t chartId;
    u_int64_t createTime;
    struct list_head one_person;
    struct list_head one_message;
    struct list_head node;
}DIALOG_LIST;


#endif //!SQLITE_UTILITY_H