#ifndef __MY_FORMAT_H
#define __MY_FORMAT_H

#include <pthread.h>
#include <sys/types.h>

#ifdef __cplusplus
extern "C"
{
#endif //!__cplusplus

//为了避免不同机器编译后的结构体大小不同，所以把大的类型放前面

/**
 * 协议描述：使用常见的TLV格式数据
 *    T:2个字节，L:4字节，V:根据L而定
 * 类型字段 T:
 *    见MessageType
 * 注意： 
 *    数据的传输不包含'\0'
 */
#pragma pack(4)
struct MsgLen
{
    unsigned int m_msg_type_len;  //消息类型的长度
    unsigned int m_msg_value_len; //面熟消息长度的长度
    unsigned int m_msg_len;       //总长度
};
extern struct MsgLen Msg;
enum MESSAGETYPE
{
    E_MSG_SUCCESS = 0,      //操作成功
    E_MSG_REGIST,       	//注册
    E_MSG_LOGIN,            //登录
    E_MSG_DEBUG,            //调试
    E_MSG_WRONG_PASSWD,     //密码错误
    E_MSG_IDENTIFY_SUCCESS,	//身份认证成功
    E_MSG_IDENTIFY_FAILED,	//身份认证失败
    E_MSG_REGIST_FILED,		//注册失败
    E_MSG_REGIST_SUCCESS,	//注册成功
    E_MSG_USER_INFO,		//用户信息,在用户登录时发送,发送的结构体中包含用户id与session
    E_MSG_SESSION,          //session
    E_MSG_WRONG_SESSION,    //session错误
    E_MSG_GET_DIALOG_LIST,  //获取会话列表
    E_MSG_GET_DIALOG_PERSON_INFO,   //获取参与会话的人员信息
    E_MSG_END_DIALOG_PERSON_INFO,   //人员信息发送完
    E_MSG_GET_HISTORY_MESSAGE,      //获取历史消息
    E_MSG_GET_LATEST_MESSAGE,       //获取最近的消息
    E_MSG_GET_DIALOG_ONE,   //一条会话信息
    E_MSG_USER_SEND_MESSAGE,//用户发送的一条消息
    E_MSG_ONE_MESSAGE,      //服务器向用户发送一天消息记录
    E_MSG_END_MESSAGE,      //服务器向用户发送消息记录完成
    E_MSG_GET_DIALOG_END,   //会话列表发送完了
    E_MSG_SERVER_BUG,       //服务器BUG
    E_MSG_TIMESTAMP,        //时间戳
    E_MSG_BEG_TIMESTAMP,    //请求时间戳
    E_MSG_BEG_FILE_LIST,    //请求文件列表
    E_MSG_GET_FILE,         //请求下载文件
    E_MSG_FILE_NAME_ERROR,  //文件名错误
    E_MSG_TRANS_EXIT,       //已经有文件在传输
    E_MSG_FILE_HEAD,        //文件头信息
    E_MSG_FILE_CONTENT,     //文件内容
    E_MSG_FILE_END,         //文件结尾
    E_MSG_FILE_TRANS_ERROR, //文件传输时遇到错误
    E_MSG_FILE_IS_DIR,      //请求是一个文件夹
    E_MSG_FILE_IS_EXEC,     //请求的是一个正在运行的可执行文件
    E_MSG_SEND_FILE,        //客户端向服务器发送文件
    E_MSG_CONTINUE,         //要求对方继续文件传输
    E_MSG
}; //!MESSAGETYPE;

void head_package(char *send_cmd, unsigned short cmd_num, unsigned int packet_len);
void head_analyze(char *p, unsigned short *cmd_num, unsigned int *packet_len);

void printClientMenu(void);


#ifdef __cplusplus
}
#endif //!__cplusplus
#endif //!__MY_FORMAT_H