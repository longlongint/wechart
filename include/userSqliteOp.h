#ifndef SQLITE_OP_H
#define SQLITE_OP_H

#include "sqlite3.h"
#include "utility.h"
#include <sys/types.h>

#define DB_NAME "wechart.db"
#define USER_TABLE_NAME "user"

#define TABLE_EXITED 1
#define TABLE_NOT_EXITED 0

#define my_sqlite3_open(dbname,db) \
({\
    pthread_mutex_lock(&sqlite3OpenMutex);\
    sqlite3_open(dbname,db);\
})
#define my_sqlite3_close(db) \
({\
    pthread_mutex_unlock(&sqlite3OpenMutex);\
    sqlite3_close(db);\
})

//printf("%s,%s:%d:unlock\n",__FILE__,__FUNCTION__,__LINE__);
//printf("%s,%s:%d:lock\n",__FILE__,__FUNCTION__,__LINE__);


// 用户结构体 与数据表字段对应
typedef struct
{
    u_int64_t id;                    //账号
    u_int8_t name[NAME_LEN];         //姓名
    u_int8_t nickname[NAME_LEN];     //昵称
    u_int8_t password[NAME_LEN];     //密码
    u_int32_t onlineState;           //在线状态
    u_int8_t signature[STRING_ELN];  //个性签名
    u_int8_t birthday[NAME_LEN];     //生日,精确到天
    u_int8_t registerTime[NAME_LEN]; //注册时间,精确到秒
    u_int32_t hierarchy;             //类似QQ等级
    u_int8_t session[NAME_LEN];		 //session
    u_int8_t phoneNum[NAME_LEN];     //电话号码
    u_int8_t address[STRING_ELN];    //住址
} USER;
typedef enum {
    ENUM_ONLINE=0,  //有空
    ENUM_BUSY,      //忙碌
    ENUM_DO_NOT_DISTURB,    //请勿打扰
    ENUM_RIGHT_BACK,//马上回来
    ENUM_OFF_DUTY,  //下班
    ENUM_OFF_LINE,  //显示为离开
    ENUM_UNKNOW     //未知
}ONLINE_STAT;

#ifdef __cplusplus
	extern "C" {
#endif

void printUserInfo(USER *user);





/**
 * @brief  向用户表插入一条记录
 * @note   注册时默认用户为离线，等级默认为0
 * @param  id: 账号(类似于QQ号)，用户用户标识，唯一
 * @param  *name: 姓名
 * @param  *nickname: 昵称
 * @param  *passwd: 密码
 * @retval 
 */
int insertUserInfo(u_int64_t id, const char *name, const char *nickname, const char *passwd);


int deleteUserById(u_int64_t id);
int updateUserInfoById(u_int64_t id,const USER *pUser);

/**
 * @brief  通过用户id查找用户
 * @note   
 * @param  id: 
 * @param  *pUser: 保存查询的结果
 * @retval 成功返回0
 */
int searchUserById(unsigned long long id, USER *pUser);
int getUserPasswordByName(char *name,char *passwd,u_int64_t *id);
int getMaxUserId(u_int64_t *id);
int updateSession(u_int64_t id,char *session);
int isTableExit(sqlite3 *db, const char *tableName);
int createTable(sqlite3 *db, const char *tableName, const char *sql);
int getUserSessionById(u_int64_t id,char *session,int64_t *lastActiveTime);

#ifdef __cplusplus
}
#endif

#endif  //!SQLITE_OP_H

