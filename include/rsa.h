#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

struct rsaKey
{
    int m_exponent;
    int m_modulus;
    int m_bytes;
};
struct Key{
    struct rsaKey m_public;
    struct rsaKey m_private;
};

extern struct Key key;

int* encodeMessage(int len, int bytes, char* message, int exponent, int modulus);
char* decodeMessage(int len, int bytes, int* cryptogram, int exponent, int modulus);

int decodeTimestamp(void *buf,u_int32_t packet_len,
        u_int32_t *m_position,int64_t *m_timestamp);
int encodeTimestamp(void *buf,u_int32_t m_position,int64_t m_timestamp);