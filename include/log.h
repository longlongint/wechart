#ifndef __LOG_H
#define __LOG_H

#include <stdarg.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include "cdefs.h"


#ifdef __cplusplus
extern "C"
{
#endif  //!__cplusplus


//启用调试日志
#define USE_DEBUG_LOG
#define USE_LOG


typedef void (*log_cb)(int severity, const char *msg);
void set_log_callback(log_cb cb);
int32_t init_log_file();


/** @name Log severities
 */
/**@{*/
#define EVENT_LOG_DEBUG 0
#define EVENT_LOG_MSG   1
#define EVENT_LOG_WARN  2
#define EVENT_LOG_ERR   3
/**@}*/

/* Obsolete names: these are deprecated, but older programs might use them.
 * They violate the reserved-identifier namespace. */
#define _EVENT_LOG_DEBUG EVENT_LOG_DEBUG
#define _EVENT_LOG_MSG EVENT_LOG_MSG
#define _EVENT_LOG_WARN EVENT_LOG_WARN
#define _EVENT_LOG_ERR EVENT_LOG_ERR


void wirte_to_log(int severity, const char *msg)__THROW __nonnull ((2));
void warn_helper(int severity,const char *file,const char *func,int line,
        const char *fmt, va_list ap);
void _add_logx(const char *fmt, ...) CHECK_FMT(1, 2);
void _add_debug_logx(const char *file,const char *func,int line,const char *fmt, ...) CHECK_FMT(4, 5);
void event_errx(int eval, const char *fmt, ...) CHECK_FMT(2, 3);

#ifdef USE_LOG
#define add_log(format,...) _add_logx(format,##__VA_ARGS__)
#else
#define add_log(x) do {;} while (0)
#endif

#ifdef USE_DEBUG_LOG
#define add_debug_log(format, ...) _add_debug_logx(__FILE__,__FUNCTION__, __LINE__, format,##__VA_ARGS__)
#else
#define add_debug_log(x) do {;} while (0)
#endif

#ifdef __cplusplus
}
#endif  //!__cplusplus
#endif  //!__LOG_H
