#ifndef SERV_SOCKET_HANDLE_H
#define SERV_SOCKET_HANDLE_H

#include <sys/types.h>
#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/listener.h>
#include <event2/util.h>
#include <event2/event.h>
#include "user.h"

#ifdef __cplusplus
extern "C"
{
#endif  //!__cplusplus


#ifndef MAX_BUF_SIZE
#define MAX_BUF_SIZE 1024
#endif

int create_tcp_sockfd(int16_t tcpPort);
int create_udp_sockfd(int16_t tcpPort);
void udp_read_cb(int sockfd, short event, void *arg);
void new_connect_cb(struct evconnlistener *listener, evutil_socket_t fd,
                    struct sockaddr *sa, int socklen, void *user_data);
void tcp_event_cb(struct bufferevent *bev, short events, void *arg);
void tcp_read_cb(struct bufferevent *bev, void *arg);
void *read_cb_thread(void *arg);
int deal_server_recv(SOCK_BUFFER *bev,u_int16_t cmd,u_int32_t packet_len,char *buf);
int user_confirmation(SOCK_BUFFER *bev,void *buf,u_int32_t packet_len);
int send_session(SOCK_BUFFER *bev);
int send_current_path_file_list(SOCK_BUFFER *bev);
int send_file_to_client(SOCK_BUFFER *bev, uint32_t user_num, char *file_name);
int file_task_init(uint32_t user_num,const char *name);
int send_file_head_to_client(SOCK_BUFFER *bev, FILE_TRANS_TASK *file_task);
int save_client_file_content(char *buf, uint32_t packet_len, uint32_t user_num);
int check_client_file( uint32_t user_num);
int save_client_file_head(uint32_t user_num, char *buf, uint32_t packet_len,SOCK_BUFFER *bev);
int continue_send(uint32_t user_num,SOCK_BUFFER *bev);
void del_user(SOCK_BUFFER *bev);
int sock_buffer_read(SOCK_BUFFER *bev,char *buf,size_t size);
int sock_buffer_write(SOCK_BUFFER *bev, char *buf, size_t size);
int sendUserInfo2Client(u_int64_t id,SOCK_BUFFER *bev);
int is_session_correct(char *session, u_int64_t *Id);
int sendDialogList(SOCK_BUFFER *bev,u_int64_t id);
int sendDialogPersonInfo(SOCK_BUFFER *bev,u_int64_t userId,u_int64_t chartId);
int sendHistoryMessage(SOCK_BUFFER *bev,u_int64_t userId,u_int64_t chartId);


#ifdef __cplusplus
}
#endif  //!__cplusplus
#endif //!SOCKET_HANDLE_H
