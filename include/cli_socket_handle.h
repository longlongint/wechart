#ifndef CLI_SOCKET_HANDLE_H
#define CLI_SOCKET_HANDLE_H

#include "userSqliteOp.h"


#ifdef __cplusplus
extern "C"
{
#endif  //!__cplusplus

#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "serv_socket_handle.h"
#include "format.h"

int connect_file_trans_serv(struct sockaddr_in *servAddr, int32_t socket_type);
int client_login(int sockfd, int32_t socket_type, struct sockaddr_in *servAddr,
                 char *user_name, char *user_passwd);
void socket_read(int sockfd, int32_t socket_type, struct sockaddr_in *serv_addr);
int deal_client_recv(int sockfd, int32_t socket_type, struct sockaddr_in *serv_addr,
                     uint16_t cmd_num, uint32_t packet_len, char *buf);
int stdin_read(int sockfd, int32_t socket_type, struct sockaddr_in *serv_addr);
int tcp_udp_write(int sockfd, int32_t socket_type, struct sockaddr_in *serv_addr,char *buf,uint32_t data_len);
int send_cmd_server(int sockfd, int32_t socket_type, struct sockaddr_in *serv_addr,uint16_t cmd_num);
int send_get_cmd_to_server(int sockfd, int32_t socket_type, struct sockaddr_in *serv_addr,
                           uint16_t cmd_num, char *file_name);
int client_file_path_init(const char *name);
int send_file_to_server(int sockfd, int32_t socket_type, struct sockaddr_in *serv_addr,char *file_name);
int continue_send_file_to_server(int sockfd, int32_t socket_type, struct sockaddr_in *serv_addr);
int save_file_head(int sockfd, int32_t socket_type, struct sockaddr_in *serv_addr,
                   char *buf, uint32_t packet_len);
int send_file_head_to_server(int sockfd, int32_t socket_type, struct sockaddr_in *serv_addr);
int getDialogListFromServer(int sockfd, int32_t socket_type, struct sockaddr_in *serv_addr, char *pChartId,USER *user);
int chart_stdin_read(int sockfd, int32_t socket_type, struct sockaddr_in *serv_addr);


#ifdef __cplusplus
}
#endif  //!__cplusplus

#endif //!CLI_SOCKET_HANDLE_H
