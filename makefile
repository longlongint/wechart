ROOT=.
EXTRALIBS=-pthread
include $(ROOT)/Make.defines.linux

DIRS = source_c
PROGS = server client


all:subModule $(PROGS)
	echo $@.$$$$	#当前进程PID

subModule:
	for i in $(DIRS); do \
		(cd $$i && echo "making $$i" && $(MAKE) ) || exit 1; \
	done

#export LD_LIBRARY_PATH=./lib:$LD_LIBRARY_PATH
server:server.c
	$(CC) $(CFLAGS) $@.c -o $@ $(LDFLAGS) $(LDLIBS) -luserSqliteOp -lutility -llog -lnet -L$(ROOT)/$(EVENET_LIB_PATH) -levent
client:client.c
	$(CC) $(CFLAGS) $@.c -o $@ $(LDFLAGS) $(LDLIBS) -luserSqliteOp -lutility -llog -lnet -L$(ROOT)/$(EVENET_LIB_PATH) -levent

%:%.c
	$(CC) $(CFLAGS) -o $@ $@.c $(LIBS) $(INCDIR) $(LIBDIR)
line:
	find ./source_c -name "*.c" -exec cat {} \; -o -name "*.h" -exec cat {} \;|sed '/^$$/d'|wc -l
clean:
	for i in $(DIRS); do \
		(cd $$i && echo "cleaning $$i" && $(MAKE) clean) || exit 1; \
	done
	rm -f $(PROGS) *.log ./data/*
