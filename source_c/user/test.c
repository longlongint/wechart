#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>           /* For O_* constants */

#include "userSqliteOp.h"
#include "utility.h"
#include "log.h"
#include "user.h"

int main()
{
    int ret = initDatabase(DB_NAME);
    UTIL_ASSERT(ret == SUCCESS);

    deleteUserById(0);
    ret = insertUserInfo(0, "root", "root", "root");
    USER user;
    ret = searchUserById(0, &user);
    if (ret == 0)
    {
        printf("id:%llu\n", (unsigned long long)user.id);
        printf("name:%s\n", user.name);
    }
    else
    {
        printf("search failed\n");
    }
    memcpy(user.phoneNum,"12345678910",11);
    ret = updateUserInfoById(0,&user);
    ret = searchUserById(0, &user);
    if (ret == 0)
    {
        printf("id:%llu\n", (unsigned long long)user.id);
        printf("name:%s\n", user.name);
        printf("phoneNum:%s\n", user.phoneNum);
    }
    else
    {
        printf("search failed\n");
    }
    return 0;
}
