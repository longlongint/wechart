#include "format.h"
#include <assert.h>

#include <stdlib.h>
#include <stdio.h>

/* 消息头长度结构体，记录各字段长度 */
struct MsgLen Msg = {sizeof(short),sizeof(unsigned int),sizeof(short)+sizeof(unsigned int)};


/** 
 * @brief  格式化发送头
 * @note   暂时没有考虑字节序
 * @param  *send_cmd:   数据首地址
 * @param  cmd_num:     命令编号
 * @param  packet_len:  数据包长度(不包括数据头)
 * @retval None
 */
void  head_package(char *send_cmd,unsigned short cmd_num,unsigned int packet_len){
    int i=0;
	//命令长度 ，小端模式存放
    send_cmd[i++] = cmd_num & 0xff;
    send_cmd[i++] = (cmd_num >>8) & 0xff;
	send_cmd[i++] = packet_len & 0xff;
	send_cmd[i++] = (packet_len >> 8 ) & 0xff;
	send_cmd[i++] = (packet_len >> 16) & 0xff;
	send_cmd[i++] = (packet_len >> 24) & 0xff;
}
/** 
 * @brief  解析包头
 * @note   暂时没有考虑字节序
 * @param  *p: 
 * @param  *cmd_num: 
 * @param  *packet_len: 
 * @retval None
 */
void head_analyze(char *p,unsigned short *cmd_num,unsigned int *packet_len)
{
    assert(p != NULL);

    *cmd_num=(p[0]&0xff) | ((p[1]<<8)&0xff);
    *packet_len= (p[2]&0xff)                 | 
                ( (p[3]<<8)&0xff00  )       |
                ((p[4]<<16)&0xff0000  )     |
                ((p[5]<<24)&0xff000000);
}


void printClientMenu(void)
{
	system("clear");
	fprintf(stderr,"-------------weChart clinet--------------------\n");
	fprintf(stderr,"- 当前状态:正常在线\t\t\t      -\n");
	fprintf(stderr,"- exit/quit/q:退出程序\t\t\t      -\n");
	fprintf(stderr,"- help:显示命令菜单    \t\t\t      -\n");
	fprintf(stderr,"- info:显示用户信息      \t\t      -\n");
	fprintf(stderr,"- dialog [id]:显示对话列表[加入/创建会话id]   -\n");
	fprintf(stderr,"---------------------------------------end-----\n");
	fprintf(stderr,"\rinput command>");
}







