
#include "utility.h"
#include <stdio.h>


int
util_vsnprintf(char *buf, ssize_t buflen, const char *format, va_list ap)
{
	int r;
	if (!buflen)
		return 0;
#ifdef _MSC_VER
	r = _vsnprintf(buf, buflen, format, ap);
	if (r < 0)
		r = _vscprintf(format, ap);
#elif defined(sgi)
	/* Make sure we always use the correct vsnprintf on IRIX */
	extern int      _xpg5_vsnprintf(char * __restrict,
		__SGI_LIBC_NAMESPACE_QUALIFIER size_t,
		const char * __restrict, /* va_list */ char *);

	r = _xpg5_vsnprintf(buf, buflen, format, ap);
#else
	r = vsnprintf(buf, buflen, format, ap);
#endif
	buf[buflen-1] = '\0';
	return r;
}

int
util_snprintf(char *buf, ssize_t buflen, const char *format, ...)
{
	int r;
	va_list ap;
	va_start(ap, format);
	r = util_vsnprintf(buf, buflen, format, ap);
	va_end(ap);
	return r;
}
