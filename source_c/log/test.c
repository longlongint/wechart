#include "log.h"
#include "utility.h"

int main()
{
    /* 1.设置日志信息 */
    int err = init_log_file();
    UTIL_ASSERT(err == 0);

    add_log("this is a %s\n","test message");
    add_debug_log("this is a %s\n","test message");

    return 0;
}

