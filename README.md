---
---
# 1、练习说明
> 题目：简易的实时聊天软件

### 一、功能需求
- 1.实现注册查询、查询、修改、删除用户功能
- 2.实现1:1,1:n聊天
- 3.注意软件架构


### 二、开发工具
- 不限

### 三、成果物输出
- 1.编译的可运行成果物(附带演示/测试展示文稿)
- 2.开发源码(源码中不能留下作者个人信息)
- 3.设计文档(开发设计思路)/测试文档(测试方案设计，测试用例，测试评估)；

----
----
# 2.编译库文件
### 1.编译libevent库(没编译过的话)
```bash
cd ./source_c/libevent-2.0.20-stable
#install architecture-independent files in PREFIX [/usr/local]
./configure --prefix=$(pwd)/../../lib/libevent
make
make install
```
### 2.编译sqlite库(没编译过的话)
```bash
cd ./source_c/sqlite-autoconf-3280000
#install architecture-independent files in PREFIX [/usr/local]
./configure --prefix=$(pwd)/../../lib/sqlite
make
make install
```
----
----

# 3、代码演示

### 1、代码结构与编译
> ```make line```: 统计有效代码行数<br>
> ```tree```: 查看代码结构<br>
> ```make```: 编译<br>
> - ![编译演示](./编译.gif)

### 2、功能演示
#### 1、用户注册与登录
- ![用户登录](./用户登录.gif)
#### 2、聊天
- ![聊天](./聊天.gif)

